#ifndef _GLSHADER_HPP_
#define _GLSHADER_HPP_

#include <GL/glew.h>

GLuint LoadShader(const char *vertex_path, const char *fragment_path);

#endif
