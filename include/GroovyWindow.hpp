#ifndef _GROOVYWINDOW_HPP_
#define _GROOVYWINDOW_HPP_

#include "../include/monitor.hpp"
#include <GL/glew.h>
#include <GL/glx.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <vector>

#define VT_SHADER     "shaders/simple.vs"
#define GROOVY_SHADER "shaders/groovy.fs"

class GroovyWindow {
	Display *            display;
	Window               window;
	Screen *             screen;
	int                  screenId;
	XEvent               ev;
	XVisualInfo *        visual;
	GLXFBConfig          bestFbc;
	XSetWindowAttributes windowAttribs;
	GLXContext           context = 0;
	GLuint               program;

	Monitors *monitors;

	bool rooted = false;

  private:
	void        Draw();
	void        Root();
	void        Open();
	void        GlxVersion();
	void        GetDimensions();
	void        SendEvent();
	void        StaticUniforms();
	GLXFBConfig GetConfig();

  public:
	GroovyWindow();
	~GroovyWindow();
};

#endif
