#ifndef _FISH_HPP_
#define _FISH_HPP_

#include "Vector2.hpp"
#include <vector>

#define FISH_COUNT 10

class Fish {
	Vector2 origin;
	Vector2 pos;
	Vector2 dir;
	float   velocity;
	Vector2 accel;

  public:
	Fish();
	float *move(float level);
};

class Fishes {
	std::vector<Fish> fishes;

  public:
	Fishes();
	float *move(float level);
};

#endif
