#ifndef _FIFO_HPP_
#define _FIFO_HPP_

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string>

#define FIFO_PATH "/tmp/music.fifo"

class MpdFifo {
	int   fifo_id;
	float last = 0;

  public:
	MpdFifo();
	~MpdFifo();
	float level();
};

#endif
