#ifndef _MONITOR_HPP_
#define _MONITOR_HPP_

#include <X11/X.h>
#include <X11/Xlib.h>
#include <vector>

#include "../include/Vector2.hpp"

struct MonitorDetails {
	Vector2 dimension;
	Vector2 position;
};

class Monitors {

  public:
	std::vector<MonitorDetails> monitors;
	Vector2                     fullDimension;
	Monitors(Display *display, Screen *screen, Window window);
	~Monitors();
	float *AsArray();
};

#endif
