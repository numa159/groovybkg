#ifndef _VECTOR2_HPP_
#define _VECTOR2_HPP_

#define FLOAT_RESOLUTION 10000
#define PI               3.1416

float randf();

class Vector2 {
  public:
	Vector2 operator=(Vector2 vec);
	Vector2 operator=(float real);
	Vector2 operator+(float real);
	Vector2 operator+(Vector2 vec);
	Vector2 operator+=(Vector2 vec);
	Vector2 operator*=(float real);
	Vector2 operator*(Vector2 vec);
	Vector2 operator*(float real);

	void randomize();
	void normalize();
	void rotate(float radians);

	float length();
	float rotation();

	float x;
	float y;

	void debug(const char *vecName = "Vector");

	Vector2(float x = 0.f, float y = 0.f);
};

#endif
