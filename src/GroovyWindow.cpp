#include <chrono>
#include <cstring>
#include <iostream>

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xrandr.h>

#include <GL/glew.h>
#include <GL/glx.h>

#include "../include/Fifo.hpp"
#include "../include/Fish.hpp"
#include "../include/GLShader.hpp"

#include "../include/GroovyWindow.hpp"

#define TITLE      "GroovyWin"
#define BAR_HEIGHT 25
#define DOBLE_BAR  True

#define WIDTH  1280
#define HEIGHT 720

float BaseColor[] = {.0, .0, .0};
/* float BaseColor[]    = {.094, .102, .149}; */
float FilterMatrix[] = {
	/* 1.0, 0.0, 0.0, // */
	/* 0.0, 1.0, 0.0, // IDENTITY */
	/* 0.0, 0.0, 1.0, // */
	1.0, 0.0, 0.0, //
	0.0, 1.0, 0.0, //
	0.0, 0.0, 1.0, //
};

typedef GLXContext (*glXCreateContextAttribsARBProc)(
	Display *, GLXFBConfig, GLXContext, Bool, const int *);

void GroovyWindow::Root() {
	if (rooted)
		return;
	rooted = true;

	Atom window_type = XInternAtom(display, "_NET_WM_WINDOW_TYPE", True);
	long value = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DESKTOP", True);
	XChangeProperty(
		display, window, window_type, XA_ATOM, 32, PropModeReplace,
		(unsigned char *)&value, 1);

	Atom state = XInternAtom(display, "_NET_WM_STATE", True);

	long below = XInternAtom(display, "_NET_WM_STATE_BELOW", True);
	XChangeProperty(
		display, window, state, XA_ATOM, 32, PropModeAppend,
		(unsigned char *)&below, 1);

	long sticky = XInternAtom(display, "_NET_WM_STATE_STICKY", True);
	XChangeProperty(
		display, window, state, XA_ATOM, 32, PropModeAppend,
		(unsigned char *)&sticky, 1);

	long skip_pager = XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", True);
	XChangeProperty(
		display, window, state, XA_ATOM, 32, PropModeAppend,
		(unsigned char *)&skip_pager, 1);

	long skip_taskbar =
		XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", True);
	XChangeProperty(
		display, window, state, XA_ATOM, 32, PropModeAppend,
		(unsigned char *)&skip_taskbar, 1);
	XLowerWindow(display, window);
};

void GroovyWindow::GetDimensions() {
	monitors = new Monitors(display, screen, window);

	/* XResizeWindow( */
	/* 	display, window, monitors->fullDimension.x, monitors->fullDimension.y);
	 */
	XResizeWindow(display, window, WIDTH, HEIGHT);
	XMoveWindow(display, window, 0, 0);
	XLowerWindow(display, window);
}

GLXFBConfig GroovyWindow::GetConfig() {
	GLint glxAttribs[] = {
		GLX_X_RENDERABLE,
		True,
		GLX_DRAWABLE_TYPE,
		GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,
		GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,
		GLX_TRUE_COLOR,
		GLX_RED_SIZE,
		8,
		GLX_GREEN_SIZE,
		8,
		GLX_BLUE_SIZE,
		8,
		GLX_ALPHA_SIZE,
		8,
		GLX_DEPTH_SIZE,
		24,
		GLX_STENCIL_SIZE,
		8,
		GLX_DOUBLEBUFFER,
		True,
		None};

	int          fbcount;
	GLXFBConfig *fbc =
		glXChooseFBConfig(display, screenId, glxAttribs, &fbcount);
	if (fbc == 0) {
		std::cout << "Failed to retrieve framebuffer.\n";
		XCloseDisplay(display);
		exit(1);
	}
	std::cout << "Found " << fbcount << " matching framebuffers.\n";

	std::cout << "Getting best XVisualInfo\n";
	int best_fbc = -1, worst_fbc = -1, best_num_samp = -1, worst_num_samp = 999;
	for (int i = 0; i < fbcount; ++i) {
		XVisualInfo *vi = glXGetVisualFromFBConfig(display, fbc[i]);
		if (vi != 0) {
			int samp_buf, samples;
			glXGetFBConfigAttrib(
				display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
			glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLES, &samples);

			if (best_fbc < 0 || (samp_buf && samples > best_num_samp)) {
				best_fbc      = i;
				best_num_samp = samples;
			}
			if (worst_fbc < 0 || !samp_buf || samples < worst_num_samp)
				worst_fbc = i;
			worst_num_samp = samples;
		}
		XFree(vi);
	}
	std::cout << "Best visual info index: " << best_fbc << "\n";
	GLXFBConfig bestFbc = fbc[best_fbc];
	XFree(fbc); // Make sure to free this!
	return bestFbc;
}

void GroovyWindow::GlxVersion() {
	GLint majorGLX, minorGLX = 0;
	glXQueryVersion(display, &majorGLX, &minorGLX);
	if (majorGLX <= 1 && minorGLX < 2) {
		std::cout << "GLX 1.2 or greater is required.\n";
		XCloseDisplay(display);
		exit(1);
	} else {
		std::cout << "GLX client version: "
				  << glXGetClientString(display, GLX_VERSION) << '\n';
		std::cout << "GLX server version: "
				  << glXQueryServerString(display, screenId, GLX_VERSION)
				  << "\n";
	}
}

void GroovyWindow::SendEvent() {
	// Create client message
	XClientMessageEvent xclient;
	memset(&xclient, 0, sizeof(xclient));

	// Fill with data
	xclient.type         = ClientMessage;
	xclient.window       = window;
	xclient.message_type = 0;
	xclient.format       = 32;
	xclient.data.l[0]    = 0;

	XSendEvent(display, window, False, 0, (XEvent *)&xclient);
};

void GroovyWindow::StaticUniforms() {
	/* glUniform1i( */
	/* 	glGetUniformLocation(program, "width"), monitors->fullDimension.x); */

	/* glUniform1i( */
	/* 	glGetUniformLocation(program, "height"), monitors->fullDimension.y); */

	/* glUniform1i( */
	/* 	glGetUniformLocation(program, "monitorCount"), */
	/* 	monitors->monitors.size()); */

	glUniform1i(glGetUniformLocation(program, "width"), WIDTH);
	glUniform1i(glGetUniformLocation(program, "height"), HEIGHT);
	glUniform1i(glGetUniformLocation(program, "monitorCount"), 1);

	glUniform1i(glGetUniformLocation(program, "barHeight"), BAR_HEIGHT);
	glUniform1i(glGetUniformLocation(program, "dobleBar"), DOBLE_BAR);
	glUniform1i(glGetUniformLocation(program, "fishCount"), FISH_COUNT);

	/* float *monitorsData = monitors->AsArray(); */
	/* glUniform4fv( */
	/* 	glGetUniformLocation(program, "monitors"), */
	/* 	monitors->monitors.size() * 4, monitorsData); */

	float arr[4] = {.0, .0, WIDTH, HEIGHT};
	glUniform4fv(glGetUniformLocation(program, "monitors"), 4, arr);

	glUniform3f(
		glGetUniformLocation(program, "base"), BaseColor[0], BaseColor[1],
		BaseColor[2]);

	glUniform3fv(glGetUniformLocation(program, "filterColor"), 3, FilterMatrix);

	/* free(monitorsData); */
}

void GroovyWindow::Draw() {
	XMapRaised(display, window);
	// Set GL Sample stuff
	glClearColor(0.5f, 0.6f, 0.7f, .00f);

	std::chrono::high_resolution_clock::time_point origin =
		std::chrono::high_resolution_clock::now();
	std::chrono::high_resolution_clock::duration time;

	glUseProgram(program);
	StaticUniforms();
	int levelLoc = glGetUniformLocation(program, "level");
	int timeLoc  = glGetUniformLocation(program, "time");
	int fishLoc  = glGetUniformLocation(program, "fishData");

	MpdFifo fifo;
	float   level = fifo.level();

	Fishes fishes;

	bool rendering = true;
	while (rendering) {
		XNextEvent(display, &ev);
		if (ev.type == Expose) {
			XWindowAttributes attribs;
			XGetWindowAttributes(display, window, &attribs);
			glViewport(0, 0, attribs.width, attribs.height);
		}
		/* glClear(GL_COLOR_BUFFER_BIT); */

		std::chrono::high_resolution_clock::time_point update =
			std::chrono::high_resolution_clock::now();
		time = update - origin;

		float time_val = time.count() / 1e9;
		glUniform1f(timeLoc, time_val);

		level = .125 * fifo.level() + .8 * level;

		glUniform1f(levelLoc, level);

		float *fishData = fishes.move(level);
		glUniform4fv(fishLoc, FISH_COUNT, fishData);

		glBegin(GL_TRIANGLES);

		glColor3f(0.f, 0.f, 0.f);

		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, 1.f, 0.0f);
		glVertex3f(1.f, 1.f, 0.0f);

		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.f, 0.0f);
		glVertex3f(-1.f, -1.f, 0.0f);

		glEnd();

		// Present frame
		glXSwapBuffers(display, window);

		if (ev.type == KeyPress) {
			KeySym k = XLookupKeysym(&(ev.xkey), 1);
			if (k == XK_Escape)
				rendering = false;
		}
		SendEvent();

		free(fishData);
	}
}

void GroovyWindow::Open() {
	glewExperimental = GL_TRUE;

	windowAttribs.border_pixel     = BlackPixel(display, screenId);
	windowAttribs.background_pixel = WhitePixel(display, screenId);
	/* windowAttribs.override_redirect = True; */
	windowAttribs.colormap = XCreateColormap(
		display, RootWindow(display, screenId), visual->visual, AllocNone);
	windowAttribs.event_mask = ExposureMask | KeyPressMask;

	window = XCreateWindow(
		display, RootWindow(display, screenId), 0, 0, 1, 1, 0, visual->depth,
		InputOutput, visual->visual,
		CWBackPixel | CWColormap | CWBorderPixel | CWEventMask, //|
		/* CWOverrideRedirect, */
		&windowAttribs);

	XStoreName(display, window, TITLE);

	glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;
	glXCreateContextAttribsARB =
		(glXCreateContextAttribsARBProc)glXGetProcAddressARB(
			(const GLubyte *)"glXCreateContextAttribsARB");

	int context_attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,
		3,
		GLX_CONTEXT_MINOR_VERSION_ARB,
		2,
		GLX_CONTEXT_FLAGS_ARB,
		GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		None};

	if (glewIsSupported("GLEW_ARB_create_context") == GL_FALSE) {
		context = glXCreateNewContext(display, bestFbc, GLX_RGBA_TYPE, 0, True);
	} else {
		context = glXCreateContextAttribsARB(
			display, bestFbc, 0, true, context_attribs);
	}
	XSync(display, False);

	glXMakeCurrent(display, window, context);
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		fprintf(stderr, "GLEW Failed to load! %s\n", glewGetErrorString(err));
		exit(1);
	}
	if (!GLEW_ARB_vertex_shader || !GLEW_ARB_fragment_shader) {
		fprintf(stderr, "No GLSL support.");
		exit(1);
	}

	// Show the window
	XClearWindow(display, window);
}

GroovyWindow::GroovyWindow() {
	display = XOpenDisplay(NULL);
	if (display == NULL) {
		std::cerr << "Could not open display" << std::endl;
		exit(1);
	}
	screen   = DefaultScreenOfDisplay(display);
	screenId = DefaultScreen(display);

	GlxVersion();
	bestFbc = GetConfig();
	visual  = glXGetVisualFromFBConfig(display, bestFbc);
	if (visual == 0) {
		std::cout << "Could not create correct visual window.\n";
		XCloseDisplay(display);
		exit(1);
	}
	if (screenId != visual->screen) {
		std::cout << "screenId(" << screenId
				  << ") does not match visual->screen(" << visual->screen
				  << ").\n";
		XCloseDisplay(display);
		exit(1);
	}

	Open();
	Root();
	GetDimensions();

	program = LoadShader(VT_SHADER, GROOVY_SHADER);

	Draw();
}

GroovyWindow::~GroovyWindow() {
	// Cleanup GLX
	glXDestroyContext(display, context);

	// Cleanup X11
	XFree(visual);
	XFreeColormap(display, windowAttribs.colormap);
	XDestroyWindow(display, window);
	XCloseDisplay(display);
}
