#include "../include/monitor.hpp"

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <cstdlib>
#include <iostream>
#include <ostream>

Monitors::Monitors(Display *display, Screen *screen, Window window) {
	fullDimension.x             = XWidthOfScreen(screen);
	fullDimension.y             = XHeightOfScreen(screen);
	XRRScreenResources *screens = XRRGetScreenResources(display, window);
	for (int i = 0;; i++) {
		XRRCrtcInfo *crtc_info =
			XRRGetCrtcInfo(display, screens, screens->crtcs[i]);
		if (crtc_info->width + crtc_info->height + crtc_info->x +
				crtc_info->y ==
			0) {
			XFree(crtc_info);
			break;
		}
		MonitorDetails monitor;
		monitor.dimension.x = 1.0f * crtc_info->width;
		monitor.dimension.y = 1.0f * crtc_info->height;
		monitor.position.x  = 1.0f * crtc_info->x;
		monitor.position.y  = 1.0f * crtc_info->y;
		monitors.push_back(monitor);
		XRRFreeCrtcInfo(crtc_info);
	}
	XRRFreeScreenResources(screens);
}

float *Monitors::AsArray() {
	size_t monCount = monitors.size();
	float *arr      = (float *)malloc(sizeof(float) * monCount * 4);

	for (size_t i = 0; i < monitors.size(); i++) {
		arr[i * 4 + 0] = monitors[i].position.x;
		arr[i * 4 + 1] = monitors[i].position.y;
		arr[i * 4 + 2] = monitors[i].dimension.x;
		arr[i * 4 + 3] = monitors[i].dimension.y;
	}

	return arr;
}

Monitors::~Monitors() { free(&monitors); }
