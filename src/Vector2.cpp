#include "../include/Vector2.hpp"
#include <cmath>
#include <iostream>
#include <ostream>

float randf() {
	return (float)(rand() % FLOAT_RESOLUTION) / (float)FLOAT_RESOLUTION;
}

void Vector2::randomize() {
	x = (randf() - .5) / .5;
	y = (randf() - .5) / .5;
}

Vector2::Vector2(float x, float y) {
	this->x = x;
	this->y = y;
}

void Vector2::rotate(float radians) {
	Vector2 base(std::sin(radians), std::cos(radians));

	Vector2 f1(base.y, -1.0 * base.x);
	Vector2 f2(base.x, base.y);

	x = x * f1.x + y * f1.y;
	y = x * f2.x + y * f2.y;
}

float Vector2::rotation() {
	Vector2 normalized(x, y);
	normalized.normalize();
	float xt  = std::asin(normalized.x);
	float yt  = std::acos(normalized.y);
	bool  nxt = xt < 0;
	bool  nyt = yt < 0;

	return std::abs(yt) * (nxt != nyt ? -1 : 1);
}

float Vector2::length() { return std::sqrt(x * x + y * y); }

void Vector2::normalize() {
	float length = this->length();
	x /= length;
	y /= length;
}

Vector2 Vector2::operator=(float real) {
	x = real;
	y = real;
	return *this;
}

Vector2 Vector2::operator=(Vector2 vec) {
	x = vec.x;
	y = vec.y;
	return *this;
}

Vector2 Vector2::operator+=(Vector2 vec) {
	x += vec.x;
	y += vec.y;
	return *this;
}

Vector2 Vector2::operator*=(float real) {
	x *= real;
	y *= real;
	return *this;
}

Vector2 Vector2::operator+(float real) {
	Vector2 res(x, y);
	res.x += real;
	res.y += real;
	return res;
}

Vector2 Vector2::operator+(Vector2 vec) {
	Vector2 res(x, y);
	res.x += vec.x;
	res.y += vec.y;
	return res;
}

Vector2 Vector2::operator*(Vector2 vec) {
	Vector2 res(x, y);
	res.x *= vec.x;
	res.y *= vec.y;
	return res;
}

Vector2 Vector2::operator*(float real) {
	Vector2 res(x, y);
	res.x *= real;
	res.y *= real;
	return res;
}

void Vector2::debug(const char *vecName) {
	std::cout << "--" << vecName << "--" << std::endl;
	std::cout << "X: " << x << std::endl;
	std::cout << "Y: " << y << std::endl;
	std::cout << "Rot: " << rotation() << std::endl;
	std::cout << "L: " << length() << std::endl;
}
