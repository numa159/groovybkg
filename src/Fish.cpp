#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <math.h>
#include <ostream>

#include "../include/Fish.hpp"

#define BASE_SPEED 0.00425

Fish::Fish() {
	origin.randomize();
	origin *= .5;

	pos.randomize();

	dir.randomize();
	dir.normalize();

	velocity = 1.;

	/* pos.debug("POSITION"); */
	/* dir.debug("DIRECTION"); */
}

float *Fish::move(float level) {
	pos = pos + dir * (velocity * BASE_SPEED);

	Vector2 toCenter = (origin + (pos * -1));
	toCenter.normalize();
	toCenter.rotate(((std::rand() % 2) * 2 - 1) * PI / 64.);

	dir += toCenter * .01;
	dir.normalize();

	/* pos.debug("POSITION"); */
	/* dir.debug("DIRECTION"); */

	float *data = (float *)malloc(sizeof(float) * 4);

	data[0] = pos.x;
	data[1] = pos.y;
	data[2] = dir.rotation() + PI;
	data[3] = velocity * (1 + level);

	return data;
}

Fishes::Fishes() {
	uint seed = std::time(nullptr);
	srand(seed);

	for (int i = 0; i < FISH_COUNT; i++) {
		Fish fish;
		fishes.push_back(fish);
	}
}

float *Fishes::move(float level) {
	float *data = (float *)malloc(sizeof(float) * 4 * fishes.size());

	for (size_t i = 0; i < fishes.size(); i++) {
		float *fishData = fishes[i].move(level);
		data[i * 4 + 0] = fishData[0];
		data[i * 4 + 1] = fishData[1];
		data[i * 4 + 2] = fishData[2];
		data[i * 4 + 3] = fishData[3];
		free(fishData);
	}

	return data;
}
