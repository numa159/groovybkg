#include "../include/Fifo.hpp"

#include <algorithm>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>

#define SIZE 1024
#define SKIP 512

MpdFifo::MpdFifo() { fifo_id = open(FIFO_PATH, O_RDONLY | O_NONBLOCK); }

float MpdFifo::level() {
	uint16_t buf[SIZE];
	read(fifo_id, &buf, sizeof(buf));

	float total = 0;

	for (int i = 0; i < SIZE - SKIP; i++) {
		total += (float)buf[SKIP + i] / (SIZE - SKIP);
	}

	return (std::max((float)abs(total) - 18000.0, 0.0)) / 64000.0;
}

MpdFifo::~MpdFifo() { close(fifo_id); }
