#!/bin/sh
name=$1
if [ -z $name ]; then
	echo "invalid name"
else
	cat ./_hppTemplate | sed "s/<++>/$(echo $name | tr '[:lower:]' '[:upper:]')/g;s/<\*\*>/<++>/g" > ./include/$name.hpp
	touch ./src/$name.cpp
fi
