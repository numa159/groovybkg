cmake_minimum_required(VERSION 3.5)

project(GroovyBkg)

execute_process(COMMAND glslify src/shaders/simple.glsl -o shaders/simple.vs)
execute_process(COMMAND glslify src/shaders/groovy.glsl -o shaders/groovy.fs)

set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror -std=c++20")
set (source_dir "${PROJECT_SOURCE_DIR}/src/")
set (OpenGL_GL_PREFERENCE "GLVND")

file (GLOB source_files "${source_dir}/*.cpp")

add_executable (GroovyBkg ${source_files})

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(X11 REQUIRED)

target_link_libraries(GroovyBkg ${OPENGL_LIBRARIES} GLEW::GLEW X11 X11::Xrandr)
include_directories(${X11_INCLUDE_DIR} ${X11_Xrandr_INCLUDE_PATH} ${GLEW_INCLUDE_DIRS})
