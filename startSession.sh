#!/bin/bash

proyectTag="GB"
proyectFolder=$HOME/Projects/groovybkg

session=$2 
default=Dev

cd $proyectFolder

currDir=$(pwd)

tmux new-session -d -s ${proyectTag}_Dev -c $currDir vim .
tmux new-session -d -s ${proyectTag}_Debug && \
tmux send 'valgrind --vgdb=yes --vgdb-error=0 --leak-check=full ./GroovyBkg' && \
tmux split-window && \
tmux send-keys C-a && \
tmux send 'gdb ./GroovyBkg'

tmux a -t $session > /dev/null 2>&1 || tmux a -t ${proyectTag}_$default
