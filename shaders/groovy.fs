#version 330 core
#define GLSLIFY 1
float hue2rgb(float f1, float f2, float hue) {
    if (hue < 0.0)
        hue += 1.0;
    else if (hue > 1.0)
        hue -= 1.0;
    float res;
    if ((6.0 * hue) < 1.0)
        res = f1 + (f2 - f1) * 6.0 * hue;
    else if ((2.0 * hue) < 1.0)
        res = f2;
    else if ((3.0 * hue) < 2.0)
        res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;
    else
        res = f1;
    return res;
}

vec3 hsl2rgb(vec3 hsl) {
    vec3 rgb;
    
    if (hsl.y == 0.0) {
        rgb = vec3(hsl.z); // Luminance
    } else {
        float f2;
        
        if (hsl.z < 0.5)
            f2 = hsl.z * (1.0 + hsl.y);
        else
            f2 = hsl.z + hsl.y - hsl.y * hsl.z;
            
        float f1 = 2.0 * hsl.z - f2;
        
        rgb.r = hue2rgb(f1, f2, hsl.x + (1.0/3.0));
        rgb.g = hue2rgb(f1, f2, hsl.x);
        rgb.b = hue2rgb(f1, f2, hsl.x - (1.0/3.0));
    }   
    return rgb;
}

vec3 hsl2rgb(float h, float s, float l) {
    return hsl2rgb(vec3(h, s, l));
}

//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : ijm
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//

vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec2 mod289(vec2 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec3 permute(vec3 x) {
  return mod289(((x*34.0)+1.0)*x);
}

float snoise(vec2 v)
  {
  const vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                      0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                     -0.577350269189626,  // -1.0 + 2.0 * C.x
                      0.024390243902439); // 1.0 / 41.0
// First corner
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);

// Other corners
  vec2 i1;
  //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
  //i1.y = 1.0 - i1.x;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  // x0 = x0 - 0.0 + 0.0 * C.xx ;
  // x1 = x0 - i1 + 1.0 * C.xx ;
  // x2 = x0 - 1.0 + 2.0 * C.xx ;
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;

// Permutations
  i = mod289(i); // Avoid truncation effects in permutation
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
    + i.x + vec3(0.0, i1.x, 1.0 ));

  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;

// Gradients: 41 points uniformly over a line, mapped onto a diamond.
// The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)

  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;

// Normalise gradients implicitly by scaling m
// Approximation of: m *= inversesqrt( a0*a0 + h*h );
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );

// Compute final noise value at P
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

in vec4 gl_FragCoord;
out vec4 FragColor;

uniform int width;
uniform int height;
uniform int monitorCount;
uniform int barHeight;
uniform int dobleBar;

uniform float time;
uniform float level = 0;

uniform vec4 monitors[10];
uniform vec4 fishData[50];
uniform int fishCount;

uniform vec3 base;
uniform vec3 filterColor[3];

float pi = 3.14159;
float e = 2.71828182846;

float hexDist(vec2 p) {
	p = abs(p);
	float c = dot(p, normalize(vec2(1,1.73)));
	c= max(c,p.x);

	return c;
}

vec4 hexUvId(vec2 uv) {
	vec2 r = vec2(1,1.73);
	vec2 h = r*.5;

	vec2 a = mod(uv, r) -h;
	vec2 b = mod(uv-h, r) -h;

	vec2 gv = dot(a, a)< dot(b, b)?a:b;
	vec2 id = uv - gv;

	vec2 hv = vec2(atan(gv.x, gv.y), hexDist(gv)/.5);

	return vec4(hv, id);
}

float bell(float num) {
	return (1./2*pi)*pow(e,-9.75*num*num);
}

vec3 applyFilter(vec3 toCol) {
	vec3 r = filterColor[0];
	vec3 g = filterColor[1];
	vec3 b = filterColor[2];

	return vec3(
		toCol.r * r.r + toCol.r * g.r + toCol.b * b.r ,
		toCol.r * r.g + toCol.g * g.g + toCol.b * b.g ,
		toCol.r * r.b + toCol.g * g.b + toCol.b * b.b 
	);
}

vec2 applyLevel(vec2 uv, float level) {
	uv.x *= 1.+level*.75;
	float antiuvx = 1.-uv.x;
	uv.y -= .1*level*bell(uv.x)*((cos(antiuvx*2.*pi * 5.)+1.) / 2.);

	return uv;
}
vec2 transformUv(vec2 uv) {
	uv -= .5;
	uv = abs(uv);
	uv = vec2(.5) - uv;
	uv.x = .5- uv.x;
	uv /= .5;

	return uv;
}

vec4 fitMonitors(vec2 position,int monitCt, vec4 mData[10], int barH, int dBar) {
	int reduction = barH  * (1+dBar);

	float threshold = 2.;

	float inBar = 0.;
	vec2 uv = vec2(.0);

	float aspectRatio = 0;

	for (int i = 0; i < monitCt; i++) {
		vec2 pos = mData[i].rg;
		vec2 dim = mData[i].ba;

		float topBar = pos.y;
		float bottomBar = dBar * (pos.y+dim.y-2.*barH);

		inBar += ((1-smoothstep(topBar-threshold, topBar, position.y))+
			dBar * smoothstep(bottomBar-threshold, bottomBar, position.y))*
			(smoothstep(pos.x, pos.x+1., position.x)*
			(1-smoothstep(pos.x+dim.x-1., pos.x+dim.x, position.x)));

		float inThisMonitor = 
			smoothstep(dim.x+pos.x+.1,dim.x+pos.x, position.x) *
			smoothstep(pos.x-.1,pos.x, position.x) *
			smoothstep(pos.y-.1,pos.y, position.y) *
			smoothstep(dim.y+pos.y-reduction+.1,dim.y+pos.y-reduction, position.y);

		aspectRatio += inThisMonitor * ( dim.x / dim.y);

		uv = inThisMonitor * 
			(position - pos)/vec2(dim.x,dim.y - reduction) + 
			(1 - inThisMonitor) * uv;
	}
	return vec4(fract(vec2(uv)),inBar, aspectRatio);
}

vec3 soundBar(vec2 uv, float level) {
	vec3 col = vec3(0.);
	uv = transformUv(uv);
	uv = applyLevel(uv, level*6.0);
	uv.y += .009;

	col += (1.-smoothstep(0.,.02, uv.y))*4.;

	float m = (1-uv.x)*(1. - smoothstep(0., .01, uv.y));
	float w = (1-uv.x)*(smoothstep(-0.01, .0 , uv.y));
	m*= w;

	float xdieoff = bell(uv.x);

	float auraZone = smoothstep(0.,.01, uv.y)*(1 - smoothstep(0., .1*xdieoff*(level/3.+.25), uv.y));
	float xpos = uv.x * 10;

	vec3 auraCol = hsl2rgb( fract((uv.x - time/2.) * 360.0 / 360.0), 1.0, .5) * level * 10.;
	vec3 aura = auraCol * auraZone;

	float boundary = clamp(1 - uv.x,0.,1.);

	col += aura * boundary;
	col += m * boundary;

	float underZone = smoothstep(1.,3.,col.x);
	col = underZone *.25* auraCol+ col *(1-underZone);

	float soundY= uv.y + 1.25 - clamp(0.,1.,time/4.);
	vec3 soundBarMask = smoothstep(0.1,.5,vec3(1. - (uv.x + soundY*soundY)));
	col *= soundBarMask;

	return col;
}

vec3 glare(vec2 uv, vec3 base, float level) {
	/* vec3 glare = vec3(.1); */
	vec2 cuv = uv-.5;
	return vec3((1 - length(cuv))) * base * (1 - level);
}

vec4 getUvi(
	vec2 res, 
	vec2 pos, 
	int mc, 
	vec4 ms[10], 
	int bh, 
	int db
) {
	pos.y *= -1.;
	pos.y -= barHeight;
	return fitMonitors(pos, mc, ms, bh, db);
}

mat2 Rot(float a) {
	float s=sin(a), c=cos(a);
	return mat2(c,-s,s,c);
}

float silhouette(float shape) {
	return smoothstep(0.,.2, smoothstep(.0,1., shape) * (1 -smoothstep(0.,1.,shape)));
}

float wing(vec2 uv) {
	float topLimit = .25;
	float botLimit = .15;
	uv.x *= -1;
	return clamp(smoothstep(topLimit,botLimit,length(uv)) -
		smoothstep(topLimit,botLimit,length(uv - vec2(.005, .125))),0.,1.);
}

float band(float val, float speed, float width) {
	return smoothstep(1.-width-.2,1.-width+.2, sin((val-speed)*5.));
}

vec2 fishHex(vec2 uv, int id) {
	vec4 hv = hexUvId(uv*35. + vec2(id));
	return vec2(smoothstep(.9,.7,length(hv.y)), snoise(hv.zw));
}

float fishGlare(vec2 uv, float speed, int id) {
	vec2 scames = fishHex(uv, id);
	float p = band(scames.y, speed, .75) *
		band(uv.y-abs(uv.x*uv.x*.2), speed*1.5, .2*(1. + .4 * abs(uv.x*uv.x*.2)));
	return scames.x * p;
}

float fishSolid(vec2 uv) {
	float Medge = .15;
	float medge = .075;
	float tpend = .3;
	float body = smoothstep(-.15,-.05,uv.y) *
		smoothstep(Medge,medge,uv.x + uv.y * tpend) * 
		smoothstep(Medge,medge,uv.x) *
		smoothstep(-Medge,-medge,uv.x - uv.y * tpend) * 
		smoothstep(-Medge,-medge,uv.x) ;

	float disp = .225;
	float hpend = .5;
	float hcutoff = .325;
	float head = smoothstep(-hcutoff,-hcutoff+.1,uv.y) *
		smoothstep(.0,.1, uv.x+uv.y*hpend+disp) *
		smoothstep(.0,.1, -uv.x+uv.y*hpend+disp) *
		smoothstep(-.05,-.15,uv.y);
	head*=head;

	vec2 tuv = uv;
	tuv.x = abs(uv.x);
	tuv.y -= .4;
	tuv*= 2.;
	tuv *= Rot(-pi/8);
	float tail = wing(tuv);

	vec2 wuv = uv;
	wuv.x = abs(uv.x) - .05;
	wuv.y += .025;
	wuv*= 2.;
	float wing = wing(wuv);

	return clamp(body + head + tail + wing,0.,1.);
}

float fishBlack(vec2 uv) {
	vec2 euv = uv;
	euv.x = abs(uv.x)-.0675;
	euv.y += .15;
	vec2 eyePos = Rot(-pi/8)*euv;

	float eyes = smoothstep(.055,-.017, length(eyePos*vec2(3.,1.)));

	float center = smoothstep(.035,-.02,abs(uv.x)) *
		smoothstep(.375,.0,abs(uv.y-.0675));

	return clamp(eyes + center,0.,1.);
}

float fishDistortion(vec2 uv, float speed, float vel) {
	float distY = (2. + clamp(uv.y*1.25 - .25, -1.,1.))* .9;
	distY*=distY;
	return sin(-1*speed*6.+distY*pi*.5)*.02 * distY;
}

float fishShape(vec2 uv) {
	float f = 1.5*(fishSolid(uv) - fishBlack(uv));
	return clamp(f*f*f*f*f,0.,1.);
}

vec3 colorFish(vec2 gv, float level, float speed, int id, float vel) {
	float tLevel = level * 5.;
	vec3 color = hsl2rgb( fract(gv.y*.125 + speed*-.25 + (id / fishCount)), 1.0, tLevel * .5);

	float shape = fishShape(gv);
	float sil = silhouette(shape);
	float hex = fishHex(gv, id).x;
	float solid = fishSolid(gv);

	return tLevel * color * ( 
			sil + shape * (.4 * fishGlare(gv, speed * 2., id) + .08 * hex)
		) + (1-tLevel) * (sil + .08 * hex * shape) + solid*solid*.01;
}

vec3 fish(vec2 uv, float speed, float level, float size, vec2 disp, float rot, float vel, int id) {
	vec2 gv = uv/size*.5;
	gv -= disp/size*.5;
	gv *= Rot(rot);
	gv.x += fishDistortion(gv, speed, vel);

	return length(gv) > 1. ? vec3(0.):colorFish(gv, level, speed, id, vel);
}

vec3 posFish(vec2 uv, float size, vec4 data, float speed, float level, int id) {
	vec2 position = data.xy * .5;
	float rotation = data.z;
	float vel = data.w;
	return fish(uv, speed, level, size, position, rotation, vel, id);
}

vec3 fishes(vec2 uv, float speed, float level, int number, vec4 data[50], float size) {
	vec3 col = vec3(0.);
	for (int i = 0; i < number; i++){
		float inoise = snoise(vec2(i)) * size*.25;
		float smthng = smoothstep(0.,.02, col.r+col.g+col.b);
		col += (1-smthng) * posFish(uv, size*.75+inoise, data[i], speed + i * .33 * pi, level, i);
	}
	return max(col,0.);
}

void main () {

	vec3 col = base * .5;

	vec2 res= vec2(width, height);
	vec2 pos = gl_FragCoord.xy - vec2(0.,res.y);

	vec4 uvi = getUvi(res, pos, monitorCount, monitors, barHeight, dobleBar);
	vec2 uv = uvi.xy;
	float inBar = uvi.z;
	float aspect = uvi.w;
	vec2 cuv = uv - .5;
	cuv.x *= aspect;

	float speed = 2*pi*time / 16.;

	col += soundBar(uv, level);
	col += glare(uv, base*.5, level);

	col += fishes(cuv, speed, level, fishCount, fishData, .15);

	col = applyFilter(col);
	col *= smoothstep(1.,0.,length(cuv)/(5.*speed));
	col = inBar * vec3(0.) + (1-inBar)*col;

	/* col = vec3(p); */
	/* col = vec3(hexUvId(cuv*5.).xy, 0.); */
	/* col = vec3(fishGlare(cuv, time, 0)); */
	FragColor = vec4( col, 1.);
}
